package com.example.misiontic19.clases;

import com.example.misiontic19.UsuarioInsertarActivity;

import junit.framework.TestCase;

public class PruebasUnitariasTest extends TestCase {

    public void testCalcularAreaTriangulo() {
        PruebasUnitarias p = new PruebasUnitarias();
        double resultado = p.calcularAreaTriangulo(12,6);

        assertEquals(36.0, resultado);
    }

    public void testVerificarUsuario()
    {
        PruebasUnitarias p = new PruebasUnitarias();
        boolean resultado = p.verificarUsuario("123", "123");
        assertEquals(false, resultado);
    }

    public void testCamposVacios()
    {
        assertEquals(true, UsuarioInsertarActivity.camposVacios("","","",""));
    }
}