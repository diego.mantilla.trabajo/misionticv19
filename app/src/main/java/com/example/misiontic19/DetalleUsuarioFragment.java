package com.example.misiontic19;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetalleUsuarioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleUsuarioFragment extends DialogFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id";

    // TODO: Rename and change types of parameters
    private int mParam1;

    public DetalleUsuarioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleUsuarioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleUsuarioFragment newInstance(int param1) {
        DetalleUsuarioFragment fragment = new DetalleUsuarioFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_detalle_usuario, container, false);
        UsuarioDAO udao = new UsuarioDAO(vista.getContext());
        Usuario u = udao.obtenerUsuario(mParam1);

        TextView txvNombres = (TextView) vista.findViewById(R.id.detalleusuario_txvNombres);
        TextView txvApellidos = (TextView) vista.findViewById(R.id.detalleusuario_txvApellidos);
        TextView txvEmail = (TextView) vista.findViewById(R.id.detalleusuario_txvEmail);
        TextView txvLatitud = (TextView) vista.findViewById(R.id.detalleusuario_txvLatitud);
        TextView txvLongitud = (TextView) vista.findViewById(R.id.detalleusuario_txvLongitud);

        txvNombres.setText("Nombres: " + u.getNombres());
        txvApellidos.setText("Apellidos: " + u.getApellidos());
        txvEmail.setText("Email: " + u.getEmail());
        txvLatitud.setText("Latitud: " + String.valueOf(u.getLatitud()));
        txvLongitud.setText("Longitud: " + String.valueOf(u.getLongitud()));

        return vista;
    }
}