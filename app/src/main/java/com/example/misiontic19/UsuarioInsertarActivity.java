package com.example.misiontic19;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.misiontic19.clases.Mensajes;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.UUID;

public class UsuarioInsertarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_insertar);

        EditText txtNombres = (EditText) findViewById(R.id.usuario_insertar_txtNombres);
        EditText txtApellidos = (EditText) findViewById(R.id.usuario_insertar_txtApellidos);
        EditText txtEmail = (EditText) findViewById(R.id.usuario_insertar_txtEmail);
        EditText txtClave = (EditText) findViewById(R.id.usuario_insertar_txtClave);
        EditText txtLatitud = (EditText) findViewById(R.id.usuario_insertar_txtLatitud);
        EditText txtLongitud = (EditText) findViewById(R.id.usuario_insertar_txtLongitud);
        Button btnGuardar = (Button) findViewById(R.id.usuario_insertar_btnGuardar);
        Button btnCancelar = (Button) findViewById(R.id.usuario_insertar_btnCancelar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombres = txtNombres.getText().toString();
                String apellidos = txtApellidos.getText().toString();
                String email = txtEmail.getText().toString();
                String clave = txtClave.getText().toString();
                String latitud = txtLatitud.getText().toString();
                String longitud = txtLongitud.getText().toString();

                if(camposVacios(nombres, apellidos, email, clave))
                    new Mensajes(view.getContext()).snackbar(view, "Digite los campos vacios.");
                else {
                    long id = insertar(txtNombres, txtApellidos, txtEmail, txtClave, txtLatitud, txtLongitud);
                    new Mensajes(view.getContext()).snackbar(view, "Se ha agregado el registro correctamente, codigo: " + String.valueOf(id));
                    onBackPressed();
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public static boolean camposVacios(String nombres, String apellidos, String email, String clave)
    {
        boolean vacios = false;
        if(
                nombres.isEmpty() ||
                apellidos.isEmpty() ||
                email.isEmpty() ||
                clave.isEmpty()
        )
            vacios = true;

        return vacios;
    }

    private long insertar(EditText nombres, EditText apellidos, EditText email, EditText clave, EditText latitud, EditText longitud)
    {
        long id = 0;

        Usuario us = new Usuario();
        us.setNombres(nombres.getText().toString());
        us.setApellidos(apellidos.getText().toString());
        us.setEmail(email.getText().toString());
        us.setClave(clave.getText().toString());
        us.setLatitud(Double.parseDouble(latitud.getText().toString()));
        us.setLongitud(Double.parseDouble(longitud.getText().toString()));

        //Insertar en Sqlite
        UsuarioDAO usdao = new UsuarioDAO(this);
        id = usdao.insertar(us);

        us.setId((int) id);

        //Insertar en Firebase Real-Time
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.setPersistenceEnabled(true);
        database.getReference().child("Usuario").child(UUID.randomUUID().toString()).setValue(us);

        //Registro de usuario
        FirebaseAuth autenticacion = FirebaseAuth.getInstance();
        autenticacion.createUserWithEmailAndPassword(us.getEmail(), us.getClave());

        return id;
    }
}