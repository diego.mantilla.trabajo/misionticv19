package com.example.misiontic19;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.misiontic19.modelos.Sitio;
import com.example.misiontic19.modelos.Usuario;
import com.example.misiontic19.viewmodels.SitioViewModel;
import com.example.misiontic19.viewmodels.UsuarioViewModel;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana5_ejemplo_fragmento2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana5_ejemplo_fragmento2 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Sitio sitioActual = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana5_ejemplo_fragmento2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana5_ejemplo_fragmento2.
     */
    // TODO: Rename and change types and number of parameters
    public static semana5_ejemplo_fragmento2 newInstance(String param1, String param2) {
        semana5_ejemplo_fragmento2 fragment = new semana5_ejemplo_fragmento2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana5_ejemplo_fragmento2, container, false);
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        EditText txtNombre = (EditText) vista.findViewById(R.id.semana5_fragmento2_txtNombre);
        EditText txtDescripcion = (EditText) vista.findViewById(R.id.semana5_fragmento2_txtDescripcion);
        EditText txtLatitud = (EditText) vista.findViewById(R.id.semana5_fragmento2_txtLatitud);
        EditText txtLongitud = (EditText) vista.findViewById(R.id.semana5_fragmento2_txtLongitud);
        Button btnGuardar = (Button) vista.findViewById(R.id.semana5_fragmento2_btnGuardar);
        Button btnEliminar = (Button) vista.findViewById(R.id.semana5_fragmento2_btnEliminar);

        SitioViewModel sitioVM = ViewModelProviders.of(getActivity()).get(SitioViewModel.class);
        sitioVM.getSitio().observe(getViewLifecycleOwner(), new Observer<Sitio>() {
            @Override
            public void onChanged(Sitio sitio) {
                sitioActual = sitio;
                if(sitio!=null) {
                    txtNombre.setText(sitio.getNombre());
                    txtDescripcion.setText(sitio.getDescripcion());
                    txtLatitud.setText(String.valueOf(sitio.getLatitud()));
                    txtLongitud.setText(String.valueOf(sitio.getLongitud()));
                }
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sitioActual = (sitioActual!=null)? sitioActual : new Sitio();
                sitioActual.setNombre(txtNombre.getText().toString());
                sitioActual.setDescripcion(txtDescripcion.getText().toString());
                sitioActual.setLatitud(Double.parseDouble(txtLatitud.getText().toString()));
                sitioActual.setLongitud(Double.parseDouble(txtLongitud.getText().toString()));

                if(sitioActual.getId()!=null)
                    database.getReference().child("Sitio").child(sitioActual.getId()).setValue(sitioActual);
                else
                {
                    String id = UUID.randomUUID().toString();
                    sitioActual.setId(id);
                    database.getReference().child("Sitio").child(id).setValue(sitioActual);
                }
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sitioActual = (sitioActual!=null)? sitioActual : new Sitio();
                if(sitioActual.getId()!=null)
                    database.getReference().child("Sitio").child(sitioActual.getId()).removeValue();

            }
        });

        return vista;
    }
}