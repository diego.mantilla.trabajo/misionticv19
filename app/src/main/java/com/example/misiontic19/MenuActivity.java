package com.example.misiontic19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.misiontic19.clases.Mensajes;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_Misiontic19_barra);
        setContentView(R.layout.activity_menu);

        Bundle extras = getIntent().getExtras();

        TextView txvEmail = (TextView) findViewById(R.id.menu_txvUsuario);
        TextView txvClave = (TextView) findViewById(R.id.menu_txvClave);
        FloatingActionButton btnMas = (FloatingActionButton) findViewById(R.id.menu_btnMas);
        FloatingActionButton btnSnackbar = (FloatingActionButton) findViewById(R.id.menu_btnSnackbar);
        FloatingActionButton btnToast = (FloatingActionButton) findViewById(R.id.menu_btnToast);
        FloatingActionButton btnCerrarSesion = (FloatingActionButton) findViewById(R.id.menu_btncerrarsesion);

        try {
            txvEmail.setText(extras.getString("email"));
            txvClave.setText(extras.getString("clave"));
        }
        catch (Exception ex)
        {
        }
        btnSnackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Mensajes(view.getContext()).snackbar(view, "Has cargado un mensaje de tipo Snackbar");
            }
        });

        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Mensajes(view.getContext()).toast("Has cargado un mensaje de tipo Toast");
            }
        });

        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog dlg = new BottomSheetDialog(view.getContext());
                dlg.setContentView(R.layout.bottom_menu);
                dlg.show();
            }
        });

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth autenticacion = FirebaseAuth.getInstance();
                autenticacion.signOut();
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater propiedades = getMenuInflater();
        propiedades.inflate(R.menu.menu_principal, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i = null;

        switch (item.getItemId())
        {
            case R.id.menu_mniAcerca: i = new Intent(this, AcercaDeActivity.class);
                break;
            case R.id.menu_mniPerfil: i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://developer.android.com/guide/components/activities/intro-activities?hl=es"));
                break;
            case R.id.menu_mniSitios: i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:3152481542"));
                break;
            case R.id.menu_mniCerrar:
                FirebaseAuth autenticacion = FirebaseAuth.getInstance();
                autenticacion.signOut();
                onBackPressed();
                break;
        }
        if(i!=null)
            startActivity(i);

        return super.onOptionsItemSelected(item);
    }
}