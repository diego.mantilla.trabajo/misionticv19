package com.example.misiontic19;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.misiontic19.clases.EjemploDialogFragment;
import com.example.misiontic19.modelos.Operacion;
import com.example.misiontic19.viewmodels.OperacionViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo1_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo1_fragmento1 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana4_ejemplo1_fragmento1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana4_ejemplo1_fragmento1.
     */
    // TODO: Rename and change types and number of parameters
    public static semana4_ejemplo1_fragmento1 newInstance(String param1, String param2) {
        semana4_ejemplo1_fragmento1 fragment = new semana4_ejemplo1_fragmento1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana4_ejemplo1_fragmento1, container, false);

        EditText txtNumero1 = (EditText) vista.findViewById(R.id.semana4_ejemplo1_fragmento1_txtNum1);
        EditText txtNumero2 = (EditText) vista.findViewById(R.id.semana4_ejemplo1_fragmento1_txtNum2);
        Button btnResultado = (Button) vista.findViewById(R.id.semana4_ejemplo1_fragmento1_btnResultado);

        btnResultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num1 = Integer.parseInt(txtNumero1.getText().toString());
                int num2 = Integer.parseInt(txtNumero2.getText().toString());
/*                EjemploDialogFragment mensaje = new EjemploDialogFragment("Titulo", "Cuerpo del mensaje");
                mensaje.show(getFragmentManager(), "Mensaje");*/

                Operacion op = new Operacion();
                op.setNumero1(num1);
                op.setNumero2(num2);

                OperacionViewModel operacionVM = ViewModelProviders.of(getActivity()).get(OperacionViewModel.class);
                operacionVM.setOperacion(op);

            }
        });

        return vista;
    }
}