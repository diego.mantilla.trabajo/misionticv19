package com.example.misiontic19.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.misiontic19.clases.SqliteConex;
import com.example.misiontic19.modelos.Usuario;

import java.util.ArrayList;
import java.util.TreeMap;

import androidx.annotation.Nullable;

public class UsuarioDAO extends SqliteConex {

    private Context contexto;

    public UsuarioDAO(@Nullable Context c)
    {
        super(c);
        this.contexto = c;
    }

    public long insertar(Usuario us)
    {
        long id = 0;

        try
        {
            SqliteConex dbc = new SqliteConex(this.contexto);
            SQLiteDatabase db = dbc.getWritableDatabase();

            ContentValues valores = new ContentValues();
            valores.put("nombres", us.getNombres());
            valores.put("apellidos", us.getApellidos());
            valores.put("email", us.getEmail());
            valores.put("clave", us.getClave());
            valores.put("latitud", us.getLatitud());
            valores.put("longitud", us.getLongitud());

            id = db.insert("usuarios", "null", valores);
        }
        catch (Exception ex)
        {

        }

        return id;
    }

    public ArrayList<Usuario> listar(@Nullable String criterioBusqueda)
    {
        SqliteConex dbc = new SqliteConex(this.contexto);
        SQLiteDatabase db = dbc.getWritableDatabase();

        String consultaSQL = "SELECT id, nombres, apellidos, email, clave, latitud, longitud FROM usuarios";

        if(criterioBusqueda!=null)
            consultaSQL += " WHERE nombres like '%" + criterioBusqueda + "%' or apellidos like '%" + criterioBusqueda + "%' or email like '%" + criterioBusqueda + "%'";

        ArrayList<Usuario> usuarios = new ArrayList<>();

        Cursor cregistros = db.rawQuery(consultaSQL, null);

        if(cregistros.moveToFirst())
            do {
               Usuario us = new Usuario();
                us.setId(cregistros.getInt(0));
                us.setNombres(cregistros.getString(1));
                us.setApellidos(cregistros.getString(2));
                us.setEmail(cregistros.getString(3));
                us.setClave(cregistros.getString(4));
                us.setLatitud(cregistros.getDouble(5));
                us.setLongitud(cregistros.getDouble(6));

                usuarios.add(us);
            } while (cregistros.moveToNext());

            cregistros.close();

            return usuarios;
    }

    public Usuario obtenerUsuario(long id)
    {
        Usuario us = null;

        SqliteConex dbc = new SqliteConex(this.contexto);
        SQLiteDatabase db = dbc.getWritableDatabase();

        String consultaSQL = "SELECT id, nombres, apellidos, email, clave, latitud, longitud FROM usuarios where id = '" + String.valueOf(id) + "'";

        try {
            Cursor cregistros = db.rawQuery(consultaSQL, null);

            if (cregistros.moveToFirst()) {
                us = new Usuario();
                us.setId(cregistros.getInt(0));
                us.setNombres(cregistros.getString(1));
                us.setApellidos(cregistros.getString(2));
                us.setEmail(cregistros.getString(3));
                us.setClave(cregistros.getString(4));
                us.setLatitud(cregistros.getDouble(5));
                us.setLongitud(cregistros.getDouble(6));
            }
            cregistros.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

        return us;
    }

    public boolean editar(Usuario us)
    {
        boolean editado = false;

        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();

        try
        {
            db.execSQL("UPDATE usuarios SET nombres = '" + us.getNombres() + "', apellidos = '" + us.getApellidos() + "', email = '" + us.getEmail() + "', clave = '" + us.getClave() + "', latitud = " + String.valueOf(us.getLatitud()) + ", longitud = " + String.valueOf(us.getLongitud()) + " WHERE id = " + String.valueOf(us.getId()));
            editado = true;
        }
        catch (Exception ex)
        {

        }

        return editado;
    }

    public boolean eliminar(long id)
    {
        boolean eliminado = false;

        SqliteConex conexion = new SqliteConex(this.contexto);
        SQLiteDatabase db = conexion.getWritableDatabase();

        try
        {
            db.execSQL("DELETE FROM usuarios" +
                    "      WHERE id = '" + String.valueOf(id) + "'");
            eliminado=true;
        }
        catch (Exception ex)
        {

        }

        return eliminado;
    }
}
