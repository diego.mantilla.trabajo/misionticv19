package com.example.misiontic19.adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.misiontic19.DetalleUsuarioFragment;
import com.example.misiontic19.MapDialogFragment;
import com.example.misiontic19.R;
import com.example.misiontic19.UsuarioEditarActivity;
import com.example.misiontic19.UsuariosActivity;
import com.example.misiontic19.clases.Mensajes;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.ViewHolderRegistro> {

    ArrayList<Usuario> registros;
    FragmentManager fragmento;

    public UsuarioAdapter(ArrayList<Usuario> r, FragmentManager f)
    {
        this.registros = r;
        this.fragmento = f;
    }

    @NonNull
    @Override
    public ViewHolderRegistro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.usuario_item, null, false);
        return new ViewHolderRegistro(vista, fragmento);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRegistro holder, int position) {
        holder.cargarDatos(registros.get(position));
    }

    @Override
    public int getItemCount() {
        return this.registros.size();
    }


    public class ViewHolderRegistro extends RecyclerView.ViewHolder {

        int id;
        TextView txtNombreCompleto;
        TextView txtEmail;
        ImageView imgMapa;


        public ViewHolderRegistro(@NonNull View itemView, @NonNull FragmentManager fragmento) {
            super(itemView);

            txtNombreCompleto = (TextView) itemView.findViewById(R.id.usuario_item_nombrecompleto);
            txtEmail = (TextView) itemView.findViewById(R.id.usuario_item_email);
            imgMapa = (ImageView) itemView.findViewById(R.id.usuario_item_btnMapa);

            ImageButton btnEditar = (ImageButton) itemView.findViewById(R.id.usuario_item_btnEditar);
            ImageButton btnEliminar = (ImageButton) itemView.findViewById(R.id.usuario_item_btnEliminar);

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(view.getContext(), UsuarioEditarActivity.class);
                    i.putExtra("id", id);
                    view.getContext().startActivity(i);
                }
            });

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder msj = new AlertDialog.Builder(view.getContext());
                    msj.setTitle("Advertencia");
                    msj.setMessage("Está a punto de eliminar un usuario, toda la información que se encuentre asociada a este, sera eliminada. ¿Desea continuar con el proceso?");
                    msj.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            UsuarioDAO db = new UsuarioDAO(view.getContext());
                            if(db.eliminar(id))
                                new Mensajes(view.getContext()).alerta("Registro Eliminado", "Se ha eliminado el registro correctamente.");
                            else
                                new Mensajes(view.getContext()).alerta("Error", "Se ha producido un error al intentar eliminar el registro.");

                            ((UsuariosActivity) view.getContext()).recreate();

                        }
                    });

                    msj.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new Mensajes(view.getContext()).alerta("Proceso cancelado", "Se ha cancelado el proceso correctamente.");
                        }
                    });
                    msj.create();
                    msj.show();



                }
            });

            imgMapa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UsuarioDAO db = new UsuarioDAO(v.getContext());
                    Usuario u = db.obtenerUsuario(id);
                    MapDialogFragment.newInstance(u.getLatitud(), u.getLongitud(), u.getNombres() + " " + u.getApellidos()).show(fragmento, null);

//                    DetalleUsuarioFragment.newInstance(id).show(fragmento, null);
                }
            });



        }

        public void cargarDatos(Usuario us)
        {
            txtNombreCompleto.setText(us.getNombres() + " " + us.getApellidos());
            txtEmail.setText(us.getEmail());
            id = us.getId();
        }

    }
}
