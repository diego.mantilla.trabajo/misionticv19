package com.example.misiontic19.adapters;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.example.misiontic19.R;
import com.example.misiontic19.modelos.Sitio;
import com.example.misiontic19.modelos.Usuario;

import java.util.ArrayList;

public class SitioListViewAdapter implements ListAdapter {

    private final ArrayList<Sitio> datos;

    public SitioListViewAdapter(ArrayList<Sitio> datos) {
        this.datos = datos;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int i) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int i) {
        return datos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView tv = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.support_simple_spinner_dropdown_item, viewGroup, false);
        tv.setText(datos.get(i).getNombre());
        tv.setContentDescription(datos.get(i).getId());
        return tv;
    }

    @Override
    public int getItemViewType(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return (datos.size()>0? datos.size():1 );
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
