package com.example.misiontic19.adapters;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.example.misiontic19.R;
import com.example.misiontic19.modelos.Usuario;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatCheckedTextView;

public class EjemploListViewAdapter implements ListAdapter {

    private final ArrayList<Usuario> datos;

    public EjemploListViewAdapter(ArrayList<Usuario> datos) {
        this.datos = datos;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int i) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int i) {
        return datos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TwoLineListItem tv = (TwoLineListItem) LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_2, viewGroup, false);
        tv.getText1().setText(datos.get(i).getNombres() + " " + datos.get(i).getApellidos());
        tv.getText2().setText(datos.get(i).getEmail());
        tv.setContentDescription(String.valueOf(datos.get(i).getId()));
        return tv;
    }

    @Override
    public int getItemViewType(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return datos.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
