package com.example.misiontic19;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.misiontic19.clases.Mensajes;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth autenticacionFirebase;
    private static final int RC_SIGN_IN = 9001;
    private String email = "";
    private String clave = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        autenticacionFirebase = FirebaseAuth.getInstance();
        //autenticacionFirebase.sendPasswordResetEmail(email);

        FirebaseUser usuario = autenticacionFirebase.getCurrentUser();
        if(usuario!=null)
            irMenu(usuario.getEmail());

        setContentView(R.layout.activity_login);

        Button btnAcceder = (Button) findViewById(R.id.login_btnAcceder);
        Button btnRegistro = (Button) findViewById(R.id.login_btnRegistrarse);
        ImageButton btnGoogle = (ImageButton) findViewById(R.id.login_btnGoogle);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarCampos()) {
                    autenticacionFirebase.createUserWithEmailAndPassword(email, clave).
                            addOnCompleteListener((Activity) view.getContext(), new OnCompleteListener<AuthResult>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful())
                                        irMenu(email);
                                    else
                                        verMensaje("Ocurrio un error al registrar el usuario");
                            }
                    });
                }
                else
                    verMensaje("Diligencie los campos en blanco.");
            }
        });

        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarCampos())
                    autenticacionFirebase.signInWithEmailAndPassword(email, clave).
                            addOnCompleteListener((Activity) view.getContext(), new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful())
                                        irMenu(email);
                                    else
                                        verMensaje("No se encontro un usuario con el email ingresado.");
                                }
                        });
            }
        });

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GoogleSignInOptions gso = new
                        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.llave_cliente_web))
                        .requestEmail()
                        .build();

                Intent signInIntent = GoogleSignIn.getClient(view.getContext(), gso).getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);




            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Activity activityAcual = this;

        if(requestCode == RC_SIGN_IN)
        {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount cuenta = task.getResult(ApiException.class);
                AuthCredential credencial = GoogleAuthProvider.getCredential(cuenta.getIdToken(), null);
                autenticacionFirebase.signInWithCredential(credencial).addOnCompleteListener(activityAcual, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            FirebaseUser usuario = autenticacionFirebase.getCurrentUser();
                            irMenu(usuario.getEmail());
                        }
                    }
                });
            }
            catch (Exception ex)
            {

            }


        }

    }

    private boolean validarCampos()
    {
        boolean camposDiligenciados = false;

        EditText txtEmail = (EditText) findViewById(R.id.login_txtEmail);
        EditText txtClave = (EditText) findViewById(R.id.login_txtClave);

        if(!txtEmail.getText().toString().isEmpty() && !txtClave.getText().toString().isEmpty())
        {
            this.email = txtEmail.getText().toString();
            this.clave = txtClave.getText().toString();
            camposDiligenciados=true;
        }

        return camposDiligenciados;
    }

    private void verMensaje(String cuerpo)
    {
        AlertDialog.Builder msj = new AlertDialog.Builder(this);
        msj.setMessage(cuerpo);
        msj.create();
        msj.show();
    }

    private void irMenu(String email)
    {
        Intent i = new Intent(this, MenuActivity.class);
        i.putExtra("email", email);
        startActivity(i);
    }

}