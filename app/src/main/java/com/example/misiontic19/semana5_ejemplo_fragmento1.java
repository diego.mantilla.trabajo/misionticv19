package com.example.misiontic19;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import com.example.misiontic19.adapters.SitioListViewAdapter;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Sitio;
import com.example.misiontic19.modelos.Usuario;
import com.example.misiontic19.viewmodels.SitioViewModel;
import com.example.misiontic19.viewmodels.UsuarioViewModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana5_ejemplo_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana5_ejemplo_fragmento1 extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana5_ejemplo_fragmento1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana5_ejemplo_fragmento1.
     */
    // TODO: Rename and change types and number of parameters
    public static semana5_ejemplo_fragmento1 newInstance(String param1, String param2) {
        semana5_ejemplo_fragmento1 fragment = new semana5_ejemplo_fragmento1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana5_ejemplo_fragmento1, container, false);
        ArrayList<Sitio> sitios = new ArrayList<>();
        ListView lista = (ListView) vista.findViewById(R.id.semana5_fragmento1_lista);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference().child("Sitio").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                sitios.clear();
                for(DataSnapshot dato : dataSnapshot.getChildren()) {
                    Sitio s = dato.getValue(Sitio.class);
                    sitios.add(s);
                }
                SitioListViewAdapter adaptador = new SitioListViewAdapter(sitios);
                lista.setAdapter(adaptador);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SitioViewModel sitiovm = ViewModelProviders.of(getActivity()).get(SitioViewModel.class);
                String id = view.getContentDescription().toString();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                database.getReference().child("Sitio").child(id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        Sitio s = snapshot.getValue(Sitio.class);
                        sitiovm.setSitio(s);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });



        return vista;
    }


}