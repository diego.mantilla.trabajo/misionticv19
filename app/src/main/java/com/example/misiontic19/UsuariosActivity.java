package com.example.misiontic19;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.misiontic19.adapters.UsuarioAdapter;
import com.example.misiontic19.dao.UsuarioDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class UsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);

        actualizarRecycler();

        FloatingActionButton btnAgregar = (FloatingActionButton) findViewById(R.id.usuarios_btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), UsuarioInsertarActivity.class);
                startActivity(i);
            }
        });

        Button btnBuscar = (Button) findViewById(R.id.usuarios_btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizarRecycler();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        actualizarRecycler();
    }

    private void actualizarRecycler()
    {
        EditText txtBusqueda = (EditText) findViewById(R.id.usuarios_txtBusqueda);
        RecyclerView recUsuarios = (RecyclerView) findViewById(R.id.usuarios_rcwUsuarios);
        recUsuarios.setLayoutManager(new LinearLayoutManager(this));
        UsuarioDAO db = new UsuarioDAO(this);
        UsuarioAdapter usAD = new UsuarioAdapter(db.listar(txtBusqueda.getText().toString()), getSupportFragmentManager());
        recUsuarios.setNestedScrollingEnabled(true);
        recUsuarios.setAdapter(usAD);
    }
}