package com.example.misiontic19;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Spinner;

import com.example.misiontic19.adapters.EjemploSpinnerAdapter;

public class AdapterSpinnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_spinner);

        Spinner spSelect = (Spinner) findViewById(R.id.adapterspinner_spinner);

        String[] datos = new String[]{ "Item 1", "Item 2" };

        EjemploSpinnerAdapter adaptador = new EjemploSpinnerAdapter(datos);
        spSelect.setAdapter(adaptador);
    }
}