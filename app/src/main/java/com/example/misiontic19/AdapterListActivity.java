package com.example.misiontic19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckedTextView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.misiontic19.adapters.EjemploListViewAdapter;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;

import java.util.ArrayList;

public class AdapterListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_list);

        ListView lista = (ListView) findViewById(R.id.adapterlist_lista);

        UsuarioDAO db = new UsuarioDAO(this);
        ArrayList<Usuario> usuarios = db.listar(null);

        EjemploListViewAdapter adaptador = new EjemploListViewAdapter(usuarios);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AppCompatCheckedTextView vista = (AppCompatCheckedTextView) view;
                if(vista.isChecked())
                    vista.setChecked(false);
                else
                    vista.setChecked(true);
            }
        });
    }
}