package com.example.misiontic19;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;
import com.example.misiontic19.viewmodels.UsuarioViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento2 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana4_ejemplo2_fragmento2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana4_ejemplo2_fragmento2.
     */
    // TODO: Rename and change types and number of parameters
    public static semana4_ejemplo2_fragmento2 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento2 fragment = new semana4_ejemplo2_fragmento2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento2, container, false);

        EditText txtNombres = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtNombres);
        EditText txtApellidos = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtApellidos);
        EditText txtEmail = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtEmail);
        EditText txtClave = (EditText) vista.findViewById(R.id.semana4_ejemplo2_fragmento2_txtClave);

        UsuarioViewModel usuarioVM = ViewModelProviders.of(getActivity()).get(UsuarioViewModel.class);
        usuarioVM.getUsuario().observe(getViewLifecycleOwner(), new Observer<Usuario>() {
            @Override
            public void onChanged(Usuario usuario) {
                txtNombres.setText(usuario.getNombres());
                txtApellidos.setText(usuario.getApellidos());
                txtEmail.setText(usuario.getEmail());
                txtClave.setText(usuario.getClave());
            }
        });



        return vista;
    }
}