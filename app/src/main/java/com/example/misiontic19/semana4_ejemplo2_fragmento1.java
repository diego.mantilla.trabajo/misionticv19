package com.example.misiontic19;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import com.example.misiontic19.adapters.EjemploListViewAdapter;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;
import com.example.misiontic19.viewmodels.UsuarioViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento1 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana4_ejemplo2_fragmento1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana4_ejemplo2_fragmento1.
     */
    // TODO: Rename and change types and number of parameters
    public static semana4_ejemplo2_fragmento1 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento1 fragment = new semana4_ejemplo2_fragmento1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento1, container, false);

        ListView lstUsuarios = (ListView) vista.findViewById(R.id.semana4_ejemplo2_fragmento1_lstUsuarios);

        UsuarioDAO dbUsuario = new UsuarioDAO(vista.getContext());
        ArrayList<Usuario> usuarios = dbUsuario.listar(null);

        EjemploListViewAdapter adaptador = new EjemploListViewAdapter(usuarios);
        lstUsuarios.setAdapter(adaptador);

        lstUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                UsuarioViewModel usuariovm = ViewModelProviders.of(getActivity()).get(UsuarioViewModel.class);
                int id = Integer.parseInt(((TwoLineListItem) view).getContentDescription().toString());
                UsuarioDAO dbusuario = new UsuarioDAO(view.getContext());
                Usuario us = dbusuario.obtenerUsuario(id);
                usuariovm.setUsuario(us);
            }
        });

        return vista;
    }
}