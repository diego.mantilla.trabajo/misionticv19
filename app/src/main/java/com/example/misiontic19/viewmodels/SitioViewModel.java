package com.example.misiontic19.viewmodels;

import com.example.misiontic19.modelos.Sitio;
import com.example.misiontic19.modelos.Usuario;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SitioViewModel extends ViewModel {

    private MutableLiveData<Sitio> sitio = new MutableLiveData<>();

    public MutableLiveData<Sitio> getSitio() {
        return sitio;
    }

    public void setSitio(Sitio sitio) {
        this.sitio.setValue(sitio);
    }
}
