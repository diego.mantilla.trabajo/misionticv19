package com.example.misiontic19.viewmodels;

import com.example.misiontic19.modelos.Usuario;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UsuarioViewModel extends ViewModel {

    private MutableLiveData<Usuario> usuario = new MutableLiveData<>();

    public MutableLiveData<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario.setValue(usuario);
    }
}
