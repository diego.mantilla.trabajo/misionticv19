package com.example.misiontic19;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.misiontic19.clases.Mensajes;
import com.example.misiontic19.clases.SqliteConex;
import com.example.misiontic19.dao.UsuarioDAO;
import com.example.misiontic19.modelos.Usuario;

public class UsuarioEditarActivity extends AppCompatActivity {

    private Usuario registroActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_editar);

        Bundle parametrosObtenidos = getIntent().getExtras();
        TextView txtNombres = (TextView) findViewById(R.id.usuario_editar_txtNombres);
        TextView txtApellidos = (TextView) findViewById(R.id.usuario_editar_txtApellidos);
        TextView txtEmail = (TextView) findViewById(R.id.usuario_editar_txtEmail);
        TextView txtClave = (TextView) findViewById(R.id.usuario_editar_txtClave);
        Button btnVolver = (Button) findViewById(R.id.usuario_editar_btnCancelar);
        Button btnGuardar = (Button) findViewById(R.id.usuario_editar_btnGuardar);

        if(parametrosObtenidos.containsKey("id"))
        {
            int id = parametrosObtenidos.getInt("id");
            UsuarioDAO db = new UsuarioDAO(this);
            this.registroActual = db.obtenerUsuario(id);

            txtNombres.setText(this.registroActual.getNombres());
            txtApellidos.setText(this.registroActual.getApellidos());
            txtEmail.setText(this.registroActual.getEmail());
            txtClave.setText(this.registroActual.getClave());

        }

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombres = txtNombres.getText().toString();
                String apellidos = txtApellidos.getText().toString();
                String email = txtEmail.getText().toString();
                String clave = txtClave.getText().toString();

                if(!camposVacios(nombres, apellidos, email, clave))
                {
                    registroActual.setNombres(nombres);
                    registroActual.setApellidos(apellidos);
                    registroActual.setEmail(email);
                    registroActual.setClave(clave);

                    UsuarioDAO db = new UsuarioDAO(view.getContext());
                    if(db.editar(registroActual))
                        new Mensajes(view.getContext()).alerta("Registro editado", "Se ha editado el registro correctamente.");
                    else
                        new Mensajes(view.getContext()).alerta("Error", "Se ha producido un error al intentar editar el registro.");
                    onBackPressed();
                }
                else
                    new Mensajes(view.getContext()).alerta("Advertencia", "Diligencie los campos vacios.");

            }
        });
    }

    public static boolean camposVacios(String nombres, String apellidos, String email, String clave)
    {
        boolean vacios = false;
        if(
                nombres.isEmpty() ||
                        apellidos.isEmpty() ||
                        email.isEmpty() ||
                        clave.isEmpty()
        )
            vacios = true;

        return vacios;
    }

}