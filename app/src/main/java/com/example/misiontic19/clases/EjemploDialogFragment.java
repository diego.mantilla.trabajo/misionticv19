package com.example.misiontic19.clases;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class EjemploDialogFragment extends DialogFragment {

    private String titulo;
    private String mensaje;
    private DialogInterface.OnClickListener opcionSi = null;
    private DialogInterface.OnClickListener opcionNo = null;

    public EjemploDialogFragment() {
    }

    public EjemploDialogFragment(String titulo, String mensaje) {
        this.titulo = titulo;
        this.mensaje = mensaje;
    }

    public EjemploDialogFragment(String titulo, String mensaje, DialogInterface.OnClickListener opcionSi) {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.opcionSi = opcionSi;
    }

    public EjemploDialogFragment(String titulo, String mensaje, DialogInterface.OnClickListener opcionSi, DialogInterface.OnClickListener opcionNo) {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.opcionSi = opcionSi;
        this.opcionNo = opcionNo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder msj = new AlertDialog.Builder(requireContext());
        msj.setTitle(this.titulo);
        msj.setMessage(this.mensaje);
        if(this.opcionSi!=null)
            msj.setPositiveButton("Si", this.opcionSi);
        if(this.opcionNo!=null)
            msj.setNegativeButton("No", this.opcionNo);

        return msj.create();
    }
}
